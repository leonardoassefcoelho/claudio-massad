<?php
    /* Template Name: Sport Page */
    get_header();
?>

<div class="sport top-header-distance">
    <?php get_template_part('template-part/sport/hero'); ?>
    <?php get_template_part('template-part/sport/content'); ?>
</div>

<?php get_footer(); ?>