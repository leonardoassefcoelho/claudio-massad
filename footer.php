        <?php wp_footer(); ?>
        <footer class="footer padding-container">
            <div class="social-networks">
                <?php if(get_field('social-networks-youtube', 'option')): ?>
                    <a class="social-link" href="<?= get_field('social-networks-youtube', 'option') ?>" aria-label="Youtube" target="_blank" rel="noopener">
                        <?= file_get_contents(get_template_directory_uri() . "/assets/youtube.svg") ?>
                    </a>
                <?php endif; ?>
                <?php if(get_field('social-networks-instagram', 'option')): ?>
                    <a class="social-link" href="<?= get_field('social-networks-instagram', 'option') ?>" aria-label="Instagram" target="_blank" rel="noopener">
                        <?= file_get_contents(get_template_directory_uri() . "/assets/instagram.svg") ?>
                    </a>
                <?php endif; ?>
                <?php if(get_field('social-networks-facebook', 'option')): ?>
                    <a class="social-link" href="<?= get_field('social-networks-facebook', 'option') ?>" aria-label="Facebook" target="_blank" rel="noopener">
                        <?= file_get_contents(get_template_directory_uri() . "/assets/facebook.svg") ?>
                    </a>
                <?php endif; ?>
				<?php if(get_field('social-networks-whats', 'option')): ?>
                    <a class="social-link" href="<?= get_field('social-networks-whats', 'option') ?>" aria-label="Whatsapp" target="_blank" rel="noopener">
                        <?= file_get_contents(get_template_directory_uri() . "/assets/whatsapp.svg") ?>
                    </a>
                <?php endif; ?>
            </div>
            <div class="info">
                <p class="copyright"><?= get_field('footer-copyright', 'option') . " " . date('Y') ?></p>
            </div>
        </footer>
		<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/015d3709-94de-403b-b27c-444c09ad8725-loader.js" ></script>
    </body>
</html>