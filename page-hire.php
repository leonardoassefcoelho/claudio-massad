<?php
/* Template Name: Contrate Page */
get_header();
?>

<div class="contrate top-header-distance">
    <?php get_template_part('template-part/hire/contact'); ?>
    <?php get_template_part('template-part/hire/form'); ?>
</div>

<?php get_footer(); ?>