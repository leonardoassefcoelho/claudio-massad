<?php $domain = strpos(site_url(),'localhost') ? "//localhost" : site_url(); ?>
<amp-state id="search">
	<script type="application/json">
	    {
  			"term": "",
  			"toggle" : false
	    }
	</script>
</amp-state>
<div class="dynamic-search container" [class]="search.toggle ? 'dynamic-search container open' : 'dynamic-search container'">
	<div class="search" [class]="search.term == '' ? 'search' : 'search top'">
		<input type="text" name="s" placeholder="<?= __('Pesquisar','claudio_massad') ?>" aria-label="<?= __('Pesquisar','claudio_massad') ?>" on="change:AMP.setState({ search : { term : event.value } })" [value]="search.term">
		<div class="search-icon">
			<?= file_get_contents( get_template_directory_uri() . '/assets/search.svg' ) ?>
		</div>
	</div>
	<amp-list
		class="searched-posts"
		items="."
		src="<?= $domain ?>/wp-admin/admin-ajax.php?action=get_searched_posts&nonce=<?= wp_create_nonce('search_posts') ?>"
		[src]="search.term != '' ? ('<?= $domain ?>/wp-admin/admin-ajax.php?action=get_searched_posts&term=' + search.term + '&nonce=<?= wp_create_nonce('search_posts') ?>') : '<?= $domain ?>/wp-admin/admin-ajax.php?action=get_searched_posts&nonce=<?= wp_create_nonce('search_posts') ?>'"
		height="1"
		[hidden]="search.term == ''"
		[is-layout-container]="search.term != ''"
		binding="no"
	>
		<template type="amp-mustache">
			{{#items}}
			    <?php get_template_part('template-part/common/dynamic-post-card') ?>
		    {{/items}}
		    {{#show_more}}
		    	<a class="search-see-more" href="{{show_more_link}}"><?= __('Ver Mais','claudio_massad') ?></a>
		    {{/show_more}}
		    {{^items}}
		    	<p class="search-not-found"><?= __('Nenhum resultado encontrado.','claudio_massad') ?> <span><?= __('Verifique se digitou corretamente.','claudio_massad') ?></span></p>
		    {{/items}}
		</template>
		<div aria-label="overflow" overflow></div>
	</amp-list>
</div>