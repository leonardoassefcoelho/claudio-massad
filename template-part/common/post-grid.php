<?php
	$grid_query = new WP_Query( array( 'post_type' => 'post' , 'posts_per_page' => 6 ) );
	$domain = strpos(site_url(),'localhost') ? "//localhost/claudio" : site_url();
	$url = $domain . "/wp-admin/admin-ajax.php?action=get_grid_posts&nonce=" . wp_create_nonce('grid_posts');
	if( is_tag() ) {
		$url = $url . "&tag=" . get_queried_object()->slug;
	}
	elseif( is_category() ) {
		$url = $url . "&cat=" . get_queried_object()->slug;
	}
	elseif( is_search() ) {
		$url = $url . "&term=" . get_search_query();
	}
	elseif( is_author() ) {
		$url = $url . "&author=" . get_the_author_meta('ID');
	}
?>
<div class="grid-wrapper">
	<amp-state id="grid_filter">
		<script type="application/json">
			{
				"page" : 0,
				"dynamic" : false,
				"order" : "date"
			}
		</script>
	</amp-state>
	<amp-state id="posts_found"
		src="<?= $url ?>"
		[src]="grid_filter.dynamic ? ( '<?= $url ?>&page=' + grid_filter.page + '&order=' + grid_filter.order ) : '<?= $url ?>'"
	></amp-state>
	<div class="post-grid-info <?= is_search() ? 'search-page' : '' ?>">
		<div class="order-by">
			<select aria-label="Ordenar por" on="change:AMP.setState({ grid_filter : { order : event.value, page : 1, dynamic : true } })">
				<option disabled selected><?= __('Ordenar por','claudio_massad') ?></option>
				<option value="date"><?= __('Mais recentes','claudio_massad') ?></option>
				<option value="views"><?= __('Mais vistos','claudio_massad') ?></option>
				<option value="comments"><?= __('Mais comentados','claudio_massad') ?></option>
			</select>
		</div>
		<p class="found-results"><?= $grid_query->found_posts . " " . __('Resultados Encontrados','claudio_massad') ?></p>
	</div>
	<div class="post-grid" [hidden]="grid_filter.page == 1">
		<?php
			while( $grid_query->have_posts() ): $grid_query->the_post();
				get_template_part('template-part/common/post-card');
			endwhile;
			wp_reset_postdata();
		?>
	</div>
	<?php if( $grid_query->max_num_pages > 1 ): ?>
		<div class="show-more fixed-container">
		    <button [hidden]="grid_filter.dynamic" class="show-more-button cta-btn" on="tap:AMP.setState({ grid_filter : { dynamic : true, page: grid_filter.page == 0 ? 2 : grid_filter.page + 1 } })" aria-label="<?= __('Ver mais','claudio_massad') ?>"><?= __('Ver mais','claudio_massad') ?></button>
		</div>
	<?php endif; ?>
	<?php if( is_archive() || (is_search() || is_page_template('page-blog.php') && $grid_query->found_posts > 0) ): ?>
		<amp-list
			class="post-grid dynamic"
			[class]="grid_filter.dynamic ? 'post-grid dynamic active' : 'post-grid dynamic'"
			layout="fixed-height"
			width="auto"
			height="1px"
			load-more="manual"
			src="<?= $url ?>"
			[src]="posts_found.url"
			[is-layout-container]="grid_filter.dynamic ? true : false"
			binding="no"
			reset-on-refresh
		>
			<!-- <div placeholder class="loader-wrapper"><div class="loader"></div></div> -->
			<template type="amp-mustache">
				<?php get_template_part('template-part/common/dynamic-post-card'); ?>
			</template>
			<amp-list-load-more load-more-button class="show-more fixed-container">
		        <button class="show-more-button cta-btn" aria-label="<?= __('Ver mais','claudio_massad') ?>"><?= __('Ver mais','claudio_massad') ?></button>
		    </amp-list-load-more>
		</amp-list>
	<?php endif; ?>
	<?php if( $grid_query->found_posts == 0 ): ?>
		<div class="posts-not-found fixed-container">
			<p class="title"><?= get_field('static-search-not-found-title', 'option') ?></p>
			<p class="description"><?= get_field('static-search-not-found-description', 'option') ?></p>
			<div class="instructions">
			<?php
				if(have_rows('static-search-not-found-repeater', 'option')):
					while(have_rows('static-search-not-found-repeater', 'option')) : the_row();
			?>
				<p>- <?= get_sub_field('static-search-not-found-instructions') ?></p>
			<?php
					endwhile;
				endif;
			?>
			</div>
		</div>
	<?php endif; ?>
</div>