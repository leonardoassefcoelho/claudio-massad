<?php
    global $wp_query;
    if( is_archive() ):
        $term = get_queried_object();
        $term_name = $term->name;
        if( is_tag() ):
            $page_type = __('Tag','claudio_massad');
            $term_name = "#" . $term_name;
        else:
            $page_type = __('Categoria','claudio_massad');
        endif;
    elseif( is_search() ):
        $page_type = __('Resultados de Busca','claudio_massad');
        $term_name = get_search_query();
        $found_posts = $wp_query->found_posts;
    endif;
?>
<div class="post-grid-header fixed-container">
    <h2 class="title"><?= $page_type ?></h2>
    <h3 class="subtitle"><?= $term_name ?></h3>
    <?php if ( is_search() ): ?>
        <p class="found-posts"><?= $found_posts . ( $found_posts == 1 ? ( ' ' . __('resultado encontrado','claudio_massad') ) : ( ' ' . __('resultados encontrados','claudio_massad') ) )?></p>
        <form class="search-form" method="get" action="<?= home_url()?>">
            <input class="text-input" type="text" name="s" placeholder="<?= __('Pesquisar','claudio_massad')?>" aria-label="<?= __('Pesquisar','claudio_massad')?>">
            <label class="submit-input" for="header-grid-input" aria-label="<?= __('Buscar','claudio_massad')?>">
                <input type="submit" value="" id="header-grid-input">
            </label>
        </form>
    <?php endif; ?>
</div>