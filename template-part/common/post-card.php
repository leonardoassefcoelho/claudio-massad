<?php
    $category = get_the_category();
    $category_link = get_category_link($category[0]->cat_ID);
    $category_name = $category[0]->cat_name;
    $featured_img = claudio_massad_get_image_attributes( get_post_thumbnail_id(), 'full' );
?>
<div class="post-card">
    <a href="<?= get_the_permalink() ?>" class="link-card-img">
        <amp-img
            class="card-img"
            layout="responsive"
            width="300"
            height="200"
            src="<?= $featured_img['src'] ?>"
            <?= $featured_img['srcset'] ? 'srcset="' . $featured_img['srcset'] . '"' : '' ?>
            alt="<?= $featured_img['alt'] ?>"
            title="<?= $featured_img['title'] ?>"
        ></amp-img>
    </a>
    <div class="card-content">
        <a class="category-btn" href="<?= $category_link ?>"><?= $category_name ?></a>
        <a href="<?= get_the_permalink() ?>" class="link-card-info">
            <h3 class="title"><?= get_the_title() ?></h3>
            <p class="excerpt"><?= get_the_excerpt() ?></p>
        </a>
    </div>
</div>