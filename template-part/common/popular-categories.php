<?php if( is_null(get_field('popular-categories-show','option')) || get_field('popular-categories-show','option') ): ?>
	<section class="popular-categories container">
		<h2 class="section-title"><?= get_field('popular-categories-title','option') ?></h2>
		<p class="section-description"><?= get_field('popular-categories-description','option') ?></p>
		<div class="categories">
			<?php 
				$categories = claudio_massad_get_popular_categories();
				foreach ($categories as $category):
			?>
				<a class="category-card" href="<?= get_term_link($category->term_id) ?>">
					<?php
						$category_img = get_field('category-img','category_' . $category->term_id);
					    $category_img = claudio_massad_get_image_attributes( $category_img['ID'], 'full' );
					?>
					<amp-img
						class="category-img"
						layout="responsive"
						width="281"
						height="240"
						src="<?= $category_img['src'] ?>"
						<?= $category_img['srcset'] ? 'srcset="' . $category_img['srcset'] . '"' : '' ?>
						alt="<?= $category_img['alt'] ?>"
						title="<?= $category_img['title'] ?>"
					></amp-img>
					<p class="category-name"><?= $category->name ?></p>
				</a>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif; ?>