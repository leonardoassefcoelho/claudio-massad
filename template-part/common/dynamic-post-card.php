<div class="post-card fixed-container dynamic">
    <a href="{{post_link}}" class="link-card-img">
        {{#image_srcset}}
            <amp-img
                class="card-img"
                layout="responsive"
                width="300"
                height="200"
                src="{{image_src}}"
                srcset="{{image_srcset}}"
                alt="{{image_alt}}"
                title="{{image_title}}"
            ></amp-img>
        {{/image_srcset}}
        {{^image_srcset}}
            <amp-img
                class="card-img"
                layout="responsive"
                width="300"
                height="200"
                src="{{image_src}}"
                alt="{{image_alt}}"
                title="{{image_title}}"
            ></amp-img>
        {{/image_srcset}}
    </a>
    <div class="card-content">
        <a href="{{post_category_link}}" class="category-btn">{{{post_category}}}</a>
        <a href="{{post_link}}" class="link-card-info">
            <h3 class="title">{{{post_title}}}</h3>
            <p class="excerpt">{{{post_excerpt}}}</p>
        </a>
    </div>
</div>