<?php if (is_null(get_field('gallery-gallery-show')) || get_field('gallery-gallery-show')) : ?>
    <section class="gallery">

        <?php
        $images = get_field('gallery-gallery');
        if ($images) : ?>
            <ul>
                <?php
                foreach ($images as $image_id) :
                    $image = claudio_massad_get_image_attributes($image_id['id'], 'full');
                ?>
                    <li class="img-wrapper">
                        <amp-img class="hero-img" layout="fill" lightbox="gallery" src="<?= $image['src'] ?>" <?= $image['srcset'] ? 'srcset="' . $image['srcset'] . '"' : '' ?> alt="<?= $image['alt'] ?>" title="<?= $image['title'] ?>"></amp-img>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>

    </section>
<?php endif; ?>