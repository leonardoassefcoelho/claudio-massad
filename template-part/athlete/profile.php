<?php if (is_null(get_field('athlete-profile-show')) || get_field('athlete-profile-show')) : ?>
    <section class="athlete-profile padding-container">
        <h2 class="section-title"><?= get_field('athlete-profile-title') ?></h2>
        <div class="profile-wrapper">
            <?php
            if (get_field('athlete-profile-image')) :
                $profile_image = claudio_massad_get_image_attributes(get_field('athlete-profile-image')['id'], 'full');
            ?>
                <div class="img-wrapper">
                    <amp-img class="hero-img" layout="fill" data-amp-auto-lightbox-disable src="<?= $profile_image['src'] ?>" <?= $profile_image['srcset'] ? 'srcset="' . $profile_image['srcset'] . '"' : '' ?> alt="<?= $profile_image['alt'] ?>" title="<?= $profile_image['title'] ?>"></amp-img>
                </div>
            <?php endif; ?>
            <?php if (have_rows('athlete-profile-repeater')) : ?>
                <div class="items-wrapper">
                    <?php while (have_rows('athlete-profile-repeater')) : the_row(); ?>
                        <div class="item">
                            <p class="content"><strong><?= get_sub_field('athlete-profile-question') ?></strong> <?= get_sub_field('athlete-profile-answer') ?></p>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>