<?php if (is_null(get_field('athlete-hero-show')) || get_field('athlete-hero-show')) : ?>
    <section class="athlete-hero">
        <?php
            $athlete_image = claudio_massad_get_image_attributes(get_post_thumbnail_id(), 'full');
        ?>
        <div class="athlete-hero-wrapper">
            <amp-img class="hero-img" layout="fill" data-amp-auto-lightbox-disable src="<?= $athlete_image['src'] ?>" <?= $athlete_image['srcset'] ? 'srcset="' . $athlete_image['srcset'] . '"' : '' ?> alt="<?= $athlete_image['alt'] ?>" title="<?= $athlete_image['title'] ?>"></amp-img>
        </div>

        <div class="content-wrapper">
            <?= get_field('athlete-hero-title'); ?>
        </div>

    </section>
<?php endif; ?>