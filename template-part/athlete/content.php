<?php if (is_null(get_field('athlete-content-show')) || get_field('athlete-content-show')) : ?>
    <section class="athlete-content padding-container">
        <?php if (have_rows('athlete-content-repeater')) : ?>
            <?php while (have_rows('athlete-content-repeater')) : the_row(); ?>
                <div class="content-wrapper">
                    <?php if (get_sub_field('athlete-content-title')) : ?>
                        <div class="subtitle"><?= get_sub_field('athlete-content-title') ?></div>
                    <?php endif; ?>
                    <div class="content"><?= get_sub_field('athlete-content-text') ?></div>
                    <?php
                    if (get_sub_field('athlete-content-image')) :
                        $content_image = claudio_massad_get_image_attributes(get_sub_field('athlete-content-image')['id'], 'full');
                    ?>
                        <div class="img-wrapper">
                            <amp-img class="hero-img" layout="fill" src="<?= $content_image['src'] ?>" <?= $content_image['srcset'] ? 'srcset="' . $content_image['srcset'] . '"' : '' ?> alt="<?= $content_image['alt'] ?>" title="<?= $content_image['title'] ?>"></amp-img>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </section>
<?php endif; ?>