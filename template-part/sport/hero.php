<?php if (is_null(get_field('sport-hero-show')) || get_field('sport-hero-show')) : ?>
    <section class="sport-hero">
        <?php
            $sport_image = claudio_massad_get_image_attributes(get_post_thumbnail_id(), 'full');
        ?>
        <div class="sport-hero-wrapper">
            <amp-img class="hero-img" layout="fill" data-amp-auto-lightbox-disable src="<?= $sport_image['src'] ?>" <?= $sport_image['srcset'] ? 'srcset="' . $sport_image['srcset'] . '"' : '' ?> alt="<?= $sport_image['alt'] ?>" title="<?= $sport_image['title'] ?>"></amp-img>
        </div>

        <div class="content-wrapper">
            <?= get_field('sport-hero-title'); ?>
        </div>

    </section>
<?php endif; ?>