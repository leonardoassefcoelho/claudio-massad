<?php if (is_null(get_field('sport-content-show')) || get_field('sport-content-show')) : ?>
    <section class="sport-content padding-container">
        <?php if (have_rows('sport-content-repeater')) : ?>
            <?php while (have_rows('sport-content-repeater')) : the_row(); ?>
                <div class="content-wrapper">
                    <?php if (get_sub_field('sport-content-title')) : ?>
                        <div class="subtitle"><?= get_sub_field('sport-content-title') ?></div>
                    <?php endif; ?>
                    <div class="content"><?= get_sub_field('sport-content-text') ?></div>
                    <?php
                    if (get_sub_field('sport-content-image')) :
                        $content_image = claudio_massad_get_image_attributes(get_sub_field('sport-content-image')['id'], 'full');
                    ?>
                        <div class="img-wrapper">
                            <amp-img class="hero-img" layout="fill" data-amp-auto-lightbox-disable src="<?= $content_image['src'] ?>" <?= $content_image['srcset'] ? 'srcset="' . $content_image['srcset'] . '"' : '' ?> alt="<?= $content_image['alt'] ?>" title="<?= $content_image['title'] ?>"></amp-img>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </section>
<?php endif; ?>