<?php if( is_null(get_field('home-hero-show')) || get_field('home-hero-show') ): ?>
<section class="home-hero">
	<?php if( have_rows('home-hero-repeater') ): ?>
	<amp-inline-gallery layout="container">
		<amp-base-carousel class="hero-carousel" id="home-hero-carousel" layout="fixed-height" height="270" loop="true">
			<?php 
						while( have_rows('home-hero-repeater') ): the_row(); 
							$hero_image = claudio_massad_get_image_attributes( get_sub_field('home-hero-img')['id'], 'full' );
					    ?>
			<div class="home-hero-item">
				<?php if(get_sub_field('home-hero-link')): ?>
				<a href="<?= get_sub_field('home-hero-link') ?>" class="hero-img-wrapper">
					<?php endif; ?>
					<amp-img class="hero-img" layout="fill" src="<?= $hero_image['src'] ?>"
						<?= $hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '' ?>
						alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>"></amp-img>
					<div class="hero-content">
						<h2><?= get_sub_field('home-hero-text') ?></h2>
					</div>
					<?php if(get_sub_field('home-hero-link')): ?>

				</a>

				<?php endif; ?>
			</div>
			<?php
						endwhile; 
					?>
			<div slot="next-arrow" class="carousel-button next" aria-label="Próximo"></div>
			<div slot="prev-arrow" class="carousel-button prev" aria-label="Anterior"></div>
		</amp-base-carousel>
	</amp-inline-gallery>
	<?php endif; ?>
</section>
<?php endif; ?>