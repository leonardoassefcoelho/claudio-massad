<?php if (is_null(get_field('home-sponsors-show')) || get_field('home-sponsors-show')) : ?>
    <section class="home-sponsors padding-container">

        <h2 class="section-title"><?= get_field('home-sponsors-title') ?></h2>

        <?php if (have_rows('home-sponsors-repeater')) : ?>
            <?php
            while (have_rows('home-sponsors-repeater')) : the_row();
                $sponsors_image = claudio_massad_get_image_attributes(get_sub_field('home-sponsors-img')['id'], 'full');
            ?>
                <div class="sponsor-wrapper">
                    <?php if (get_sub_field('home-sponsors-link')) : ?>
                        <a href="<?= get_sub_field('home-sponsors-link') ?>" class="hero-img-wrapper">
                        <?php endif; ?>
                        <amp-img class="hero-img" layout="fill" src="<?= $sponsors_image['src'] ?>" <?= $sponsors_image['srcset'] ? 'srcset="' . $sponsors_image['srcset'] . '"' : '' ?> alt="<?= $sponsors_image['alt'] ?>" title="<?= $sponsors_image['title'] ?>"></amp-img>
                        </a>
                        <?php if (get_sub_field('home-sponsors-link')) : ?>
                            </a>
                        <?php endif; ?>
                </div>
            <?php
            endwhile;
            ?>
        <?php endif; ?>
    </section>
<?php endif; ?>