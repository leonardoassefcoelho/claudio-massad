<?php if (is_null(get_field('home-history-show')) || get_field('home-history-show')) : ?>
    <section class="home-history padding-container">
        <?php $history_image = claudio_massad_get_image_attributes(get_field('home-history-img')['id'], 'full'); ?>
        <div class="img-wrapper">
            <amp-img class="meet-img" layout="fill" src="<?= $history_image['src'] ?>" <?= $history_image['srcset'] ? 'srcset="' . $history_image['srcset'] . '"' : '' ?> alt="<?= $history_image['alt'] ?>" title="<?= $history_image['title'] ?>">
            </amp-img>
        </div>

        <div class="section-title"><?= get_field('home-history-title') ?></div>

        <div class="content-wrapper">
            <?= get_field('home-history-text') ?>
        </div>

        <?php if (get_field('home-history-cta-title')) : ?>
            <a class="btn" href="<?= get_field('home-history-cta-url') ?>"><?= get_field('home-history-cta-title') ?></a>
        <?php endif; ?>
    </section>
<?php endif; ?>