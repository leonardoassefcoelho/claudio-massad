<?php if( is_null( get_field('home-recent-show') ) || get_field('home-recent-show') ): ?>
    <section class="recent-posts padding-container">
        <h2 class="section-title"><?= get_field('home-recent-title') ?></h2>
        <?php
            $recents_args = array(
                'posts_per_page' => 4,
                'ignore_sticky_posts' => 1
            );
            $recents_query = new WP_Query($recents_args);
            if( $recents_query->have_posts() ):
        ?>
                <div class="posts-wrapper">
                <?php 
                    while( $recents_query->have_posts() ): $recents_query->the_post(); 
                ?>
                        <?php get_template_part('template-part/common/post-card') ?>
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
                </div>
        <?php endif; ?>
        <?php if (get_field('home-meet-cta-title')) : ?>
            <a class="btn" href="<?= get_field('home-meet-cta-url') ?>"><?= get_field('home-meet-cta-title') ?></a>
        <?php endif; ?>
    </section>
<?php endif; ?>