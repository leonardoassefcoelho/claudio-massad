<?php if( is_null(get_field('home-testimonials-show')) || get_field('home-testimonials-show') ): ?>
	<section class="testimonials padding-container">
        <h2 class="section-title"><?= get_field('home-testimonials-title') ?></h2>

		<?php if( have_rows('home-testimonials-repeater') ): ?>
			<amp-inline-gallery layout="container">
				<amp-base-carousel
					class="hero-carousel"
					id="home-testimonials-carousel"
                    layout="fixed-height"
                    height="440px"
                    loop="false"
				>
					<?php 
						while( have_rows('home-testimonials-repeater') ): the_row(); 
                        ?>
                            <div class="home-testimonials-wrapper">
                                <div class="home-testimonials-item">
									<span class="quote"><?= file_get_contents(get_template_directory_uri() . "/assets/quote.svg") ?></span>
                                    <p class="content"><?= get_sub_field('home-testimonial-content') ?></p>
                                    <p class="name"><?= get_sub_field('home-testimonial-name') ?></p>
                                    <p class="description"><?= get_sub_field('home-testimonial-description') ?></p>
                                </div>
                            </div>
					<?php
						endwhile; 
					?>
					<div slot="next-arrow" class="carousel-button next" aria-label="Próximo"></div>
    				<div slot="prev-arrow" class="carousel-button prev" aria-label="Anterior"></div>
				</amp-base-carousel>
			</amp-inline-gallery>
		<?php endif; ?>
	</section>
<?php endif; ?>