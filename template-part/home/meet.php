<?php if (is_null(get_field('home-meet-show')) || get_field('home-meet-show')) : ?>
    <section class="home-meet container">

        <amp-state id="modalStatus">
            <script type="application/json">
                {
                    "modalOpen": false
                }
            </script>
        </amp-state>

        <?php $meet_image = claudio_massad_get_image_attributes(get_field('home-meet-img')['id'], 'full'); ?>
        <div class="img-wrapper">
            <amp-img class="meet-img" layout="fill" src="<?= $meet_image['src'] ?>" <?= $meet_image['srcset'] ? 'srcset="' . $meet_image['srcset'] . '"' : '' ?> alt="<?= $meet_image['alt'] ?>" title="<?= $meet_image['title'] ?>">
            </amp-img>
        </div>

        <div class="section-title"><?= get_field('home-meet-title') ?></div>

        <div class="content-wrapper">
            <?= get_field('home-meet-text') ?>
        </div>

        <?php
        if (get_field('home-meet-video-cta-title')) :
            $video_url = get_field('home-meet-video-cta-url')['url'];
        ?>
            <a tabindex="0" role="button" class="btn video" on="tap:AMP.setState({modalStatus:{modalOpen: !modalStatus.modalOpen}})"><?= get_field('home-meet-video-cta-title') ?></a>

            <div class="modal" [class]="modalStatus.modalOpen == true ? 'modal open' : 'modal'" on="tap:AMP.setState({modalStatus:{modalOpen: !modalStatus.modalOpen}})">
                <amp-video autoplay controls width="300" height="175" layout="responsive" poster="<?= $meet_image ?>">
                    <source src="<?= $video_url ?>" type="video/mp4" />
                    <div fallback>
                        <p>o seu navegador não suporta a exibição de vídeos em HTML5.</p>
                    </div>
                </amp-video>
            </div>
        <?php endif; ?>

        <?php if (get_field('home-meet-cta-title')) : ?>
            <a class="btn" href="<?= get_field('home-meet-cta-url') ?>"><?= get_field('home-meet-cta-title') ?></a>
        <?php endif; ?>
    </section>
<?php endif; ?>