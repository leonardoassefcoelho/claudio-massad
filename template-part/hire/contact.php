<?php if (is_null(get_field('hire-contact-show')) || get_field('hire-contact-show')) : ?>
    <section class="hire-contact padding-container">
        <h1 class="section-title"><?= get_field('hire-contact-title') ?></h1>
        <div class="content-wrapper">
        <?php if (have_rows('hire-contact-repeater')) : ?>
            <?php while (have_rows('hire-contact-repeater')) : the_row(); ?>
                    <div class="card">
                        <?php if (get_sub_field('hire-contact-icon')) : ?>
                            <h2 class="icon"><?= get_sub_field('hire-contact-icon') ?></h2>
                        <?php endif; ?>
                        <?php if (get_sub_field('hire-contact-title')) : ?>
                            <h2 class="title"><?= get_sub_field('hire-contact-title') ?></h2>
                        <?php endif; ?>
                        <?php if (get_sub_field('hire-contact-description')) : ?>
                            <div class="description"><?= get_sub_field('hire-contact-description') ?></div>
                        <?php endif; ?>                        
                    </div>
            <?php endwhile; ?>
        <?php endif; ?>
        </div>
        <div class="contact">
            <h2 class="title"><?= get_field('hire-contact-infos-name') ?></h2>
            <p class="info"><strong>Site: </strong><a href="<?= get_field('hire-contact-infos-site') ?>" alt="<?= get_field('hire-contact-infos-name') ?>" title="<?= get_field('hire-contact-infos-name') ?>" target="_blank"><?= get_field('hire-contact-infos-site') ?></a></p>
            <p class="info"><strong>E-Mail: </strong><a href="mailto:<?= get_field('hire-contact-infos-email') ?>" alt="<?= get_field('hire-contact-infos-name') ?>" title="<?= get_field('hire-contact-infos-name') ?>" target="_blank"><?= get_field('hire-contact-infos-email') ?></a></p>
            <p class="info"><strong>Telefone: </strong><?= get_field('hire-contact-infos-phone') ?></p>
			<p class="info"><strong>Whatsapp: </strong><?= get_field('hire-contact-infos-whats') ?></p>
        </div>
    </section>
<?php endif; ?>