<?php if (is_null(get_field('blog-hero-show')) || get_field('blog-hero-show')) : ?>
    <section class="blog-hero">
        <?php
            $sport_image = claudio_massad_get_image_attributes(get_post_thumbnail_id(), 'full');
        ?>
        <div class="blog-hero-wrapper">
            <amp-img class="hero-img" layout="fill" data-amp-auto-lightbox-disable src="<?= $sport_image['src'] ?>" <?= $sport_image['srcset'] ? 'srcset="' . $sport_image['srcset'] . '"' : '' ?> alt="<?= $sport_image['alt'] ?>" title="<?= $sport_image['title'] ?>"></amp-img>
        </div>

        <div class="content-wrapper">
            <?= get_field('blog-hero-title'); ?>
        </div>
    </section>
<?php endif; ?>