<?php
    $author_id = get_the_author_meta('ID');
    $avatar_src = get_avatar_url($author_id, 80);
    $author_name = get_the_author_meta('display_name');
    $author_description = get_the_author_meta('description');
    $author_url = get_author_posts_url($author_id);
    $author_linkedin = get_the_author_meta('linkedin');
?>
<section class="author-bio">
    <h2 class="author-name"><?= $author_name ?></h2>
    <div class="author-wrapper">
        <amp-img
            class="author-avatar"
            layout="responsive"
            width="100"
            height="100"
            src="<?= $avatar_src ?>"
            alt="<?= $author_name ?>"
            title="<?= $author_name ?>">
        </amp-img>
        <?php if( $author_linkedin ): ?>
            <a class="author-linkedin" href="<?= $author_linkedin ?>" rel="noopener" target="_blank"><?= file_get_contents(get_template_directory_uri() . '/assets/linkedin.svg') ?></a>
        <?php endif; ?>
        <div class="author-description">
            <h3 class="title"><?= __('Biografia','claudio_massad') ?></h3>
            <p class="subtitle"><?= $author_description ?></p>
        </div>
    </div>
</section>