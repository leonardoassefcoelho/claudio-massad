<div class="single-share">
	<p class="title"><?= __('Gostou? Compartilhe!' , 'claudio_massad') ?></p>
	<div class="share-social-networks">
		<amp-social-share
			class="whatsapp-share share-social-network-ga"  
			aria-label="<?= __('Compartilhar no Whatsapp' , 'claudio_massad') ?>"
			type="whatsapp"
		    data-param-text="<?= __('Confira o artigo' , 'claudio_massad') ?>: TITLE - CANONICAL_URL"
		    data-vars-social-network="whatsapp"
			data-vars-post-url="<?= get_permalink() ?>"
			width="48"
			height="48"
		>
		</amp-social-share>
		<amp-social-share
			class="linkedin-share share-social-network-ga"  
			aria-label="<?= __('Compartilhar no Linkedin' , 'claudio_massad') ?>"
			type="linkedin"
			data-vars-social-network="linkedin"
			data-vars-post-url="<?= get_permalink() ?>"
			data-param-text="<?= __('Confira o artigo' , 'claudio_massad') ?>: TITLE - CANONICAL_URL"
			width="48"
			height="48"
		>
		</amp-social-share>
		<?php if( get_field('social-networks-facebook-id' , 'option') ): ?>
			<amp-social-share
				class="facebook-share share-social-network-ga"
				aria-label="<?= __('Compartilhar no Facebook' , 'claudio_massad') ?>"
				type="facebook"
				data-vars-social-network="facebook"
				data-vars-post-url="<?= get_permalink() ?>"
				width="48"
				height="48"
				data-param-app_id="<?= get_field('social-networks-facebook-id' , 'option'); ?>"
			>
			</amp-social-share>
		<?php endif; ?>
		<amp-social-share
			class="twitter-share share-social-network-ga"
			aria-label="<?= __('Compartilhar no Twitter' , 'claudio_massad') ?>"
			type="twitter"
			data-vars-social-network="twitter"
			data-vars-post-url="<?= get_permalink() ?>"
			data-param-text="<?= __('Confira o artigo' , 'claudio_massad') ?>: TITLE"
			width="48"
			height="48"
		>
		</amp-social-share>
	</div>
</div>