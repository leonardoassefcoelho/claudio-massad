<?php
    $author_id = get_the_author_meta('ID');
    $author_name = get_the_author_meta('display_name');
    $author_description = get_the_author_meta('description');
    $author_linkedin = get_the_author_meta('linkedin');
    $author_img = get_field('author-img', 'user_' . $author_id);
    $author_img = claudio_massad_get_image_attributes( $author_img['ID'], 'full' );
?>
<section class="single-author">
    <h2 class="section-title"><?= __('Autor' , 'tempotem') ?></h2>
    <div class="author-wrapper fixed-container">
        <amp-img
            class="author-img"
            layout="responsive"
            width="312"
            height="143"
            src="<?= $author_img['src'] ?>"
            <?= $author_img['srcset'] ? 'srcset="' . $author_img['srcset'] . '"' : '' ?>
            alt="<?= $author_img['alt'] ?>"
            title="<?= $author_img['title'] ?>">
        </amp-img>
        <div class="author-info">
            <div class="info-wrapper">
                <p class="name"><?= $author_name ?></p>
                <?php if( $author_linkedin ): ?>
                    <a class="linkedin" aria-label="linkedin" href="<?= $author_linkedin ?>" rel="noopener" target="_blank"><?= file_get_contents(get_template_directory_uri() . '/assets/linkedin.svg') ?></a>
                <?php endif; ?>
            </div>
            <p class="description"><?= $author_description ?></p>
        </div>
    </div>
</section>
