<?php
    $category = get_the_category();
    $category_link = get_category_link($category[0]->cat_ID);
    $category_name = $category[0]->cat_name;
    $thumbnail_img = claudio_massad_get_image_attributes( get_post_thumbnail_id(), 'full' );
?>
<div class="single-hero">
	<div class="content-wrapper">
		<a class="category-btn" href="<?= $category_link ?>"><?= $category_name ?></a>
		<h1 class="single-title"><?= get_the_title() ?></h1>
		<p class="single-info"><?= get_the_date() . " | Por: " . get_the_author() ?></p>
		<p class="single-excerpt"><?= get_the_excerpt() ?></p>
	</div>
    <amp-img
        class="single-thumbnail container"
        layout="responsive"
        width="300"
        height="300"
        src="<?= $thumbnail_img['src'] ?>"
        <?= $thumbnail_img['srcset'] ? 'srcset="' . $thumbnail_img['srcset'] . '"' : '' ?>
        alt="<?= $thumbnail_img['alt'] ?>"
        title="<?= $thumbnail_img['title'] ?>"
    ></amp-img>
</div>