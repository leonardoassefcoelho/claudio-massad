<amp-animation id="progress-bar" layout="nodisplay">
    <script type="application/json">
        {
            "duration": "3s",
            "fill": "both",
            "animations": [{
                "selector": ".progress-bar",
                "keyframes": [
                    {
                        "transform": "translateX(-100%)"
                    },
                    {
                        "transform": "translateX(0)"
                    }
                ]
            }]
        }
    </script>
</amp-animation>
<div class="progress-bar"></div>