<?php
	$post_id = get_the_ID();
	$args = array(
		'post__not_in' => array($post_id),
		'posts_per_page' => 3
	);
	$list_post_tags = get_the_tags($post_id);
	if( !empty($list_post_tags) ):
		$list_post_tags_id = array();
		foreach( $list_post_tags as $tag ){
			array_push($list_post_tags_id, $tag->term_id);
		}
		$args['tag__in'] = $list_post_tags_id;
	else: 
		$args['cat'] = get_the_category($post_id)[0]->term_id;
	endif;
	$related_posts_query = new WP_Query($args);
	if( $related_posts_query->have_posts() && ( $related_posts_query->found_posts >= 3 ) ): 
?>
		<div class="related-posts">
			<h2 class="section-description"><?= __('Confira os posts relacionados' , 'claudio_massad') ?></h2>
			<?php if( $related_posts_query->have_posts() ): ?>
				<div class="posts-wrapper">
					<?php 
						while( $related_posts_query->have_posts() ): $related_posts_query->the_post(); 
							get_template_part('template-part/common/post-card'); 
						endwhile; 
					?>
				</div>
			<?php endif; ?>
		</div>
<?php 
	wp_reset_postdata(); 
	endif; 
?>