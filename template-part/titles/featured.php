<?php if (is_null(get_field('titles-featured-show')) || get_field('titles-featured-show')) : ?>
    <section class="titles-featured padding-container">
        <h1 class="section-title"><?= get_field('titles-featured-title') ?></h1>
        <?php if (have_rows('titles-featured-repeater')) : ?>
            <?php while (have_rows('titles-featured-repeater')) : the_row(); ?>
                <div class="content-wrapper">
                    <?php
                    if (get_sub_field('titles-featured-image')) :
                        $titles_image = claudio_massad_get_image_attributes(get_sub_field('titles-featured-image')['id'], 'full');
                    ?>
                        <div class="img-wrapper">
                            <amp-img class="hero-img" layout="fill" data-amp-auto-lightbox-disable src="<?= $titles_image['src'] ?>" <?= $titles_image['srcset'] ? 'srcset="' . $titles_image['srcset'] . '"' : '' ?> alt="<?= $titles_image['alt'] ?>" title="<?= $titles_image['title'] ?>"></amp-img>
                        </div>
                    <?php endif; ?>
                    <?php if (get_sub_field('titles-featured-title')) : ?>
                        <div class="content"><?= get_sub_field('titles-featured-title') ?></div>
                    <?php endif; ?>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M255.989 512a10.663 10.663 0 01-7.552-3.115l-192-192c-4.093-4.237-3.975-10.99.262-15.083 4.134-3.992 10.687-3.992 14.82 0L255.988 486.25l184.448-184.448c4.237-4.093 10.99-3.975 15.083.262a10.666 10.666 0 010 14.821l-192 192a10.664 10.664 0 01-7.53 3.115z"/><path d="M255.989 512c-5.891 0-10.667-4.776-10.667-10.667V10.667C245.323 4.776 250.098 0 255.989 0s10.667 4.776 10.667 10.667v490.667c0 5.89-4.776 10.666-10.667 10.666z"/></svg>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </section>
<?php endif; ?>