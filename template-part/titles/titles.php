<?php if (is_null(get_field('titles-titles-show')) || get_field('titles-titles-show')) : ?>
    <section class="titles-titles padding-container">
        <h2 class="section-title"><?= get_field('titles-titles-title') ?></h2>
        <?php if (have_rows('titles-titles-repeater')) : ?>
            <?php while (have_rows('titles-titles-repeater')) : the_row(); ?>
                <div class="content-wrapper">
                    <h3 class="cat-title"><?= get_sub_field('titles-titles-category') ?></h3>
                    <ul>
                        <?php if (have_rows('titles-titles-cat-repeater')) : ?>
                            <?php while (have_rows('titles-titles-cat-repeater')) : the_row(); ?>
                                    <?php if (get_sub_field('titles-titles-cat-title')) : ?>
                                        <li class="content"><?= get_sub_field('titles-titles-cat-title') ?></li>
                                    <?php endif; ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </section>
<?php endif; ?>