<?php 
	get_header();
?>

<div class="author-page top-header-distance container">
	<?php
		get_template_part('template-part/author/author-bio');
		get_template_part('template-part/common/post-grid');
    ?>
</div>

<?php
	get_template_part('template-part/common/newsletter');
	get_footer(); 
?>