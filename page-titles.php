<?php
    /* Template Name: Titles Page */
    get_header();
?>

<div class="titles top-header-distance">
    <?php get_template_part('template-part/titles/featured'); ?>
    <?php get_template_part('template-part/titles/titles'); ?>
    <?php get_template_part('template-part/titles/olympic'); ?>
</div>

<?php get_footer(); ?>