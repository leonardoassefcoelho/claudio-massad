<?php 
	get_header(); 
?>

<div class="content-404 top-header-distance container">
	<div class="fixed-container">
	    <h2 class="section-title"><?= get_field('404-error-type','option') ?></h2>
		<p class="section-description"><?= get_field('404-title','option') ?></p>
		<p class="page-404-description"><?= get_field('404-description','option') ?></p>
		<a class="cta-btn" href="<?= home_url() ?>"><?= get_field('404-button-text','option') ?></a>
		<a class="cta-btn return" href="<?= wp_get_referer() ? wp_get_referer() : home_url(); ?>"><?= get_field('404-button-return-text','option') ?></a>
	</div>
</div>

<?php 
	get_template_part('template-part/common/newsletter');
	get_footer(); 
?>