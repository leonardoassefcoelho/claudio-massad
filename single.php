<?php 
	get_header();
	if( have_posts() ):
        while( have_posts() ): the_post();
?>
			<article class="single-page top-header-distance container">
				<?php get_template_part( 'template-part/single/progress-bar' ); ?>
				<header class="single-header">
					<?php get_template_part( 'template-part/single/single-hero' ); ?>
				</header>
			    <main class="main-content">
			    	<div class="content-wrapper">
				    	<div class="content">
				    	    <span class="progress-bar-anchor">
								<amp-position-observer
									intersection-ratios="0"
									on="scroll:progress-bar.seekTo(percent=event.percent)"
									layout="nodisplay"
									hidden
								></amp-position-observer>
							</span>
				    		<?php the_content(); ?>
				    	</div>
			            <?php
			                get_template_part( 'template-part/single/single-tags' );
			                get_template_part( 'template-part/single/single-share' );
			            ?>
			            <?php
			                if( comments_open() ):
                                comments_template();
			                endif;
			            ?>
		            </div>
		            <?php get_template_part( 'template-part/single/related-posts' ); ?>
			    </main>
			</article>
<?php   
		endwhile;
	endif;
	get_template_part( 'template-part/common/newsletter' );
	get_footer(); 
?>