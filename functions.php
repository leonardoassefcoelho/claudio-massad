<?php

require 'inc/custom-post-types/default-post-type.php';
require 'inc/custom-taxonomies/default-taxonomy.php';
require 'inc/utils/utils.php';
require 'inc/ajax.php';
require 'inc/comments.php';
require 'inc/setup.php';