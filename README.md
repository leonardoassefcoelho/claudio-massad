# Blog Boilerplate

O **boilerplate** foi pensado como forma de otimizar o processo de desenvolvimento de um **blog**. A ideia é sempre manter esse tema atualizado com as novidades e melhorias.

## Templates

O tema conta com os arquivos e templates comumente utilizados em um blog Wordpress (para as páginas *404*, *archive*, *author*, *comments*, *footer*, *header*, *search*, *single*). As divisões são dadas por páginas e por seções.

Commons:
 - dynamic-post-card
 - dynamic-search
 - header-post-grid
 - newsletter
 - popular-categories (*seção de categorias populares*)
 - post-card
 - post-grid
 - product-section (*seção de conversão do produto, geralmente levando ao site principal*)
 - tracking-pixels (*arquivo para implementação das tags que precisam estar visíveis em todas as páginas*)

Home:
- hero
- popular-posts
- recent-posts

Single:
- progress-bar
- related-posts
- single-author
- single-hero
- single-share
- single-tags

Author:
- author-bio

---
**ACF**

Para a sincronização dos acfs, a abordagem de [Synchronized JSON](https://www.advancedcustomfields.com/resources/synchronized-json/) é utilizada. O tema já inclui os principais ACFs que são utilizados em blog.
>  **acf-json/:** é importante que essa pasta tenha permissão para `www-data:www-data` e para escrita de outros no ambiente local (para o bom funcionamento entre git e Wordpress).
----
**COMMENTS**

O template de comments conta com uma páginação dinâmica.
>  É importante configurar os comentários pelo painel para o funcionamento correto dessa feature. Ativar **2 níveis** de comentários, divididos em **páginas de 5** comentários, mostrando a **primeira** página por padrão, exibindo os comentários **mais velhos** ao topo da página.

![image](/uploads/5ae0b82b72e7b2f0c182f535b10bb9ba/image.png)

---
**Ajax**

Se você está utilizando esse tema em *localhost/nomedapasta*, lembre-se de alterar todas as urls das chamadas ajax para *//localhost/nomedapasta*.

## Plugins

Esse tema depende dos seguintes plugins para o seu correto funcionamento:
 - [Advanced Custom Fields PRO](https://www.advancedcustomfields.com/pro/)
 - [AMP](https://amp-wp.org/)

## Tradução

O tema está preparado para receber tradução, tendo todas as strings traduzíveis.

`__('string a ser traduzida','claudio_massad')`