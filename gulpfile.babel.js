import gulp from 'gulp';
import autoprefixer from 'gulp-autoprefixer';
import browserSync from 'browser-sync';

const server = browserSync.create();

const paths = {
	styles: {
		src: 'css-dev/**/*.css',
		dest: 'css'
	}
}
export const styles = () => {
	return gulp.src(paths.styles.src)
		.pipe(autoprefixer())
		.pipe(gulp.dest(paths.styles.dest));
}

export const watch = () => {
	gulp.watch('css-dev/**/*.css', gulp.series(styles, reload));
	gulp.watch('**/*.php', reload);
}

export const serve = (done) => {
	server.init({
		proxy: "http://localhost",
		browser: "chrome"
	});
	done();
}

export const reload = (done) => {
	server.reload();
	done();
}

export default gulp.series(serve, watch);