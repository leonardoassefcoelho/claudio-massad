<?php
    /* Template Name: Athlete Page */
    get_header();
?>

<div class="athlete top-header-distance">
    <?php get_template_part('template-part/athlete/hero'); ?>
    <?php get_template_part('template-part/athlete/content'); ?>
    <?php get_template_part('template-part/athlete/profile'); ?>
</div>

<?php get_footer(); ?>