<?php 
    /* Template Name: Blog Page */
    get_header();
?>

<div class="blog top-header-distance">
    <?php get_template_part('template-part/blog/hero'); ?>
    <div class="post-grid-wrapper container">
        <?php get_template_part('template-part/common/post-grid'); ?>
    </div>
</div>

<?php get_footer(); ?>