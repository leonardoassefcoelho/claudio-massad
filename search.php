<?php 
	get_header();
?>

<amp-layout class="tracking-pixel" id="search-term-ga" data-vars-search-term="<?= get_search_query() ?>" layout="fixed" width="1" height="1"></amp-layout>
<div class="search-page top-header-distance container">
	<?php 
		get_template_part('template-part/common/header-post-grid');
		get_template_part('template-part/common/post-grid');
    ?>
</div>

<?php
	get_template_part('template-part/common/newsletter');
	get_footer(); 
?>