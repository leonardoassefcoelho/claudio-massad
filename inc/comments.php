<?php 

function claudio_massad_format_comment($comment, $args, $depth) {  ?>
	<li class='post-comment'>
		<p class="comment-author"><?= ucwords(strtolower(get_comment_author())); ?></p>
        <p class="comment-date"><?= get_comment_date('') . ' | ' . get_comment_time(); ?></p>
		<div class="comment-content">
			<p class="comment-text"><?= get_comment_text(); ?></p>
			<div class="comment-reply">
				<?php comment_reply_link( array_merge( $args, array('before' => file_get_contents(get_template_directory_uri().'/assets/reply.svg'), 'respond_id' => 'anchor_comment', 'depth' => $depth, 'max_depth' => $args['max_depth'])) ) ?>
			</div>
		</div>
	</li>
<?php 
} 

function claudio_massad_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}

add_filter( 'comment_form_fields', 'claudio_massad_move_comment_field_to_bottom' );

function claudio_massad_comment_form_open_wrap() {
    echo '<div class="form-inputs">';
}

add_action('comment_form_before_fields', 'claudio_massad_comment_form_open_wrap');

function claudio_massad_comment_form_close_wrap() {
    echo '</div>';
}

add_action( 'comment_form_after_fields', 'claudio_massad_comment_form_close_wrap' );

function claudio_massad_redirect_comments_false( $location, $commentdata ) {
	wp_send_json(
        array(
		  'message' => __('Comentário enviado com sucesso e aguardando moderação.','claudio_massad')
        ),
        200
    );
}

add_filter( 'comment_post_redirect', 'claudio_massad_redirect_comments_false', 0, 2 );

function claudio_massad_get_static_comments($post_id) {
	$number = get_option('comments_per_page');

	$number_parents = get_comments(
        array(
            'count'=> true, 
            'status' => 'approve', 
            'post_id' => $post_id,
            'parent' => 0
        )
    );

    $args = array (
        'post_id' => $post_id,
        'number' =>  $number,
        'status' => 'approve',
        'parent' => 0,

    );
    $comments = get_comments($args);
    
    foreach ($comments as $comment) {
        $children = $comment->get_children();
        $comments = array_merge($comments, $children);
    }

    $args = array(
        'type' => 'comment',
        'callback' => 'claudio_massad_format_comment',
        'reverse_top_level' => false,
    );

    wp_list_comments($args, $comments);
    if($number_parents > $number): ?>
        <div class="comments-show-more">
            <button [hidden]="comments.comments_amp_list" class="button-show-more" on="tap:AMP.setState({comments : {comments_amp_list : true, page: comments.page + 1 }}),dynamic-comment-list.changeToLayoutContainer()" aria-label="<?= __('Ver Mais','claudio_massad')?>"><?= __('Ver Mais','claudio_massad')?></button>
        </div>
    <?php endif;  
}