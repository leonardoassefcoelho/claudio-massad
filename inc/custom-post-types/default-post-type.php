<?php

function claudio_massad_create_default_cpt() {

    $labels = array(
        'name' 					=> 'Conteúdos Especiais',
        'singular_name' 		=> 'Conteúdo Especial',
        'menu_name' 			=> 'Conteúdos Especiais',
        'name_admin_bar' 		=> 'Conteúdos Especiais',
        'archives' 				=> 'Arquivo',
        'attributes' 			=> 'Atributos',
        'parent_item_colon' 	=> 'Parent event:',
        'all_items' 			=> 'Todos os conteúdos especiais',
        'add_new_item' 			=> 'Adicionar novo conteúdo especial',
        'add_new' 				=> 'Adicionar novo',
        'new_item' 				=> 'Novo conteúdo especial',
        'edit_item' 			=> 'Editar conteúdo especial',
        'update_item' 			=> 'Atualizar conteúdo especial',
        'view_item' 			=> 'Vizualizar conteúdo especial',
        'view_items' 			=> 'Vizualizar conteúdos especiais',
        'search_items' 			=> 'Buscar por conteúdos especiais',
        'not_found' 			=> 'Conteúdo Especial não encontrado', 
        'not_found_in_trash' 	=> 'Conteúdo Especial não encontrado no lixo',
        'featured_image' 		=> 'Imagem de destaque',
        'set_featured_image' 	=> 'Selecionar imagem de destaque',
        'remove_featured_image' => 'Remover imagem de destaque',
        'use_featured_image' 	=> 'Usar imagem de destaque',
        'insert_into_item' 		=> 'Inserir no conteúdo especial',
        'uploaded_to_this_item' => 'Atualizado para o conteúdo especial',
        'items_list' 			=> 'Lista de conteúdos especiais',
        'items_list_navigation' => 'Navegação por lista de conteúdos especiais',
        'filter_items_list' 	=> 'Filtrar lista de conteúdos especiais',
    );

    $args = array(
        'label' 				=> 'Conteúdos Especiais',
        'description' 			=> 'Adicionar Conteúdos Especiais',
        'labels' 				=> $labels,
        'menu_icon' 			=> 'dashicons-smiley',
        'supports' 				=> array('title', 'editor', 'custom-fields', 'post-formats', 'thumbnail', 'author', 'excerpt', 'comments', 'revisions' ),
        'taxonomies' 			=> array(),
        'public' 				=> true,
        'show_ui' 				=> true,
        'show_in_menu' 			=> true,
        'menu_position' 		=> 5,
        'show_in_admin_bar' 	=> true,
        'show_in_nav_menus' 	=> true,
        'can_export' 			=> true,
        'has_archive' 			=> false,
        'hierarchical' 			=> false,
        'exclude_from_search' 	=> false,
        'show_in_rest' 			=> true,
        'publicly_queryable' 	=> true,
        'capability_type' 		=> 'post',
        'rewrite'               => array('slug' => 'conteudos-especiais')
    );

    register_post_type( 'special-content', $args );
}

// add_action( 'init', 'claudio_massad_create_default_cpt', 0 );