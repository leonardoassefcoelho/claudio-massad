<?php

function claudio_massad_setup() {
	add_theme_support( 'title-tag' );
	add_theme_support( 'amp' );
	add_theme_support( 'post-thumbnails' );
}

add_action( 'after_setup_theme', 'claudio_massad_setup' );


function claudio_massad_enqueue() {
    wp_enqueue_style( 'common-css', get_template_directory_uri().'/css/common/common.css' );
    wp_enqueue_style( 'header-css', get_template_directory_uri().'/css/common/header.css' );
    wp_enqueue_style( 'footer-css', get_template_directory_uri().'/css/common/footer.css' );
    wp_enqueue_style( 'post-card-css', get_template_directory_uri().'/css/common/post-card.css' );
    wp_enqueue_style( 'dynamic-search-css', get_template_directory_uri().'/css/common/dynamic-search.css' );
    wp_enqueue_style( 'product-section-css', get_template_directory_uri().'/css/common/product-section.css' );
    wp_enqueue_style( 'newsletter-css', get_template_directory_uri().'/css/common/newsletter.css' );
    wp_enqueue_style( 'popular-categories-css', get_template_directory_uri().'/css/common/popular-categories.css' );

    if( is_front_page() ) {
        wp_enqueue_style( 'home-hero-css', get_template_directory_uri().'/css/home/hero.css' );
        wp_enqueue_style( 'home-recent-posts-css', get_template_directory_uri().'/css/home/recent-posts.css' );
        wp_enqueue_style( 'home-meet-css', get_template_directory_uri().'/css/home/meet.css' );
        wp_enqueue_style( 'home-history-css', get_template_directory_uri().'/css/home/history.css' );
        wp_enqueue_style( 'home-sponsors-css', get_template_directory_uri().'/css/home/sponsors.css' );
        wp_enqueue_style( 'home-testimonials-css', get_template_directory_uri().'/css/home/testimonials.css' );
    }

    if( is_page_template('page-blog.php') ) {
        wp_enqueue_style( 'blog-hero-css', get_template_directory_uri().'/css/blog/hero.css' );
        wp_enqueue_style( 'blog-grid-css', get_template_directory_uri().'/css/common/post-grid.css' );
    }

    if( is_page_template('page-sport.php') ) {
        wp_enqueue_style( 'sport-hero-css', get_template_directory_uri().'/css/sport/hero.css' );
        wp_enqueue_style( 'sport-content-css', get_template_directory_uri().'/css/sport/content.css' );
    }

    if( is_page_template('page-athlete.php') ) {
        wp_enqueue_style( 'athlete-hero-css', get_template_directory_uri().'/css/athlete/hero.css' );
        wp_enqueue_style( 'athlete-content-css', get_template_directory_uri().'/css/athlete/content.css' );
        wp_enqueue_style( 'athlete-profile-css', get_template_directory_uri().'/css/athlete/profile.css' );
    }

    if( is_page_template('page-titles.php') ) {
        wp_enqueue_style( 'titles-featured-css', get_template_directory_uri().'/css/titles/featured.css' );
        wp_enqueue_style( 'titles-titles-css', get_template_directory_uri().'/css/titles/titles.css' );
    }

    if( is_page_template('page-gallery.php') ) {
        wp_enqueue_style( 'gallery-gallery-css', get_template_directory_uri().'/css/gallery/gallery.css' );
    }

    if( is_page_template('page-hire.php') ) {
        wp_enqueue_style( 'hire-form-css', get_template_directory_uri().'/css/hire/form.css' );
        wp_enqueue_style( 'hire-contact-css', get_template_directory_uri().'/css/hire/contact.css' );
    }

    if( is_single() ) {
        wp_enqueue_style( 'single-progress-bar-css', get_template_directory_uri().'/css/single/progress-bar.css' );
        wp_enqueue_style( 'single-content-css', get_template_directory_uri().'/css/single/content.css' );
        wp_enqueue_style( 'single-hero-css', get_template_directory_uri().'/css/single/single-hero.css' );
        wp_enqueue_style( 'single-share-css', get_template_directory_uri().'/css/single/single-share.css' );
        wp_enqueue_style( 'single-tags-css', get_template_directory_uri().'/css/single/single-tags.css' );
        wp_enqueue_style( 'single-author-css', get_template_directory_uri().'/css/single/single-author.css' );
        wp_enqueue_style( 'single-comments-css', get_template_directory_uri().'/css/single/comments.css' );
        wp_enqueue_style( 'single-related-posts-css', get_template_directory_uri().'/css/single/related-posts.css' );
    }

    if( is_archive() || is_search() ) {
        wp_enqueue_style( 'header-post-grid-css', get_template_directory_uri().'/css/common/header-post-grid.css' );
        wp_enqueue_style( 'post-grid-css', get_template_directory_uri().'/css/common/post-grid.css' );
    }

    if( is_404() ) {
        wp_enqueue_style( 'page-404-css', get_template_directory_uri().'/css/common/404.css' );
    }
}

add_action( 'wp_enqueue_scripts', 'claudio_massad_enqueue' );


function claudio_massad_register_menus() {
	register_nav_menus(
        array(
            'header-menu' => 'Header Menu',
            'footer-menu' => 'Footer Menu'
        )
    );
}

add_action( 'init', 'claudio_massad_register_menus' );


function claudio_massad_filter_header_menu($nav_menu, $args) {
    global $counter_submenu;
    $counter_submenu = 0;

    if ($args->menu->slug == 'header-menu'):
        $nav_menu = preg_replace_callback('/(<li .*?class=".*?has-children.*?>.*?<a(.*?)>(.*?)<\/a>)/','claudio_massad_insert_header_submenu_action',$nav_menu);
    endif;
    return $nav_menu;
}

add_filter( 'wp_nav_menu', 'claudio_massad_filter_header_menu', 10, 2 );


function claudio_massad_insert_header_submenu_action($matches){
        global $counter_submenu;

        $matches[0] = preg_replace('/<li .*?class="(.*?)menu-item-has-children.*?><a(.*?)>(.*?)<\/a>/','<li class="$1menu-item-has-children close" [class]="submenu.open_'. $counter_submenu . ' ? \' $1menu-item-has-children open \' : \' $1menu-item-has-children close \' "><div class="open-sub-menu" on="tap:AMP.setState({submenu:{open_'. $counter_submenu .': !submenu.open_'. $counter_submenu .'}})" role="button" tabindex="0">$3</div>',$matches[0]);

        $counter_submenu++;

        return $matches[0];
}


function claudio_massad_remove_pages_from_search() {
    global $wp_post_types;
    $wp_post_types['page']->exclude_from_search = true;
}

add_action( 'init', 'claudio_massad_remove_pages_from_search' );


function claudio_massad_load_theme_textdomain() {
    load_theme_textdomain('claudio_massad');
}

add_action('after_setup_theme', 'claudio_massad_load_theme_textdomain');

add_filter( 'wpcf7_autop_or_not', '__return_false' );