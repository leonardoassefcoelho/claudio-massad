<?php

function claudio_massad_truncate_excerpt($text) {
	if( strlen($text) > 119 ) {
		$text = mb_substr( $text, 0, 120 );
		$text .= '...';
	}

	return $text;
}

add_filter('get_the_excerpt', 'claudio_massad_truncate_excerpt');


require 'functions/acf-options-page.php';
require 'functions/get-image-attributes.php';
require 'functions/get-popular-categories.php';
require 'functions/get-post-card-information.php';
require 'functions/set-categories-views.php';
require 'functions/set-posts-views.php';