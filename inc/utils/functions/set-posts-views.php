<?php 

function claudio_massad_set_post_views($post_id) {
    $count_key = 'post_view_count';
    $count = get_post_meta( $post_id, $count_key, true );

    if ( $count == '' ) {
        add_post_meta( $post_id, $count_key, '0' );
    }
    else {
        $count = intval( $count );
        $count++;
        update_post_meta( $post_id, $count_key, $count );
    }
}


function claudio_massad_track_post_views() {
    if ( is_single() ) {
        $post_id = get_the_ID();
        claudio_massad_set_post_views( $post_id );
    }
}

add_action('wp_head', 'claudio_massad_track_post_views');