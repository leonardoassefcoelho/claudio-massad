<?php 

function claudio_massad_get_popular_categories() {

	$args = array(
        'orderby' => 'meta_value_num',
        'order' => 'DESC',
        'meta_key' => 'category_view_count',
        'number' => 4,
        'hide_empty'=> false,
    );

	$current_category = get_queried_object();
	if( $current_category ) {
		$id_current_category = $current_category->term_id;
		$args['exclude'] = $id_current_category;
	}
	
	$categories = get_categories($args);

	return $categories;
	
}