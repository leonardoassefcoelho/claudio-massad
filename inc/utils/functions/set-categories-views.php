<?php 

function claudio_massad_set_category_views($category_id) {
    $count_key = 'category_view_count';
    $count = get_term_meta( $category_id, $count_key, true );

    if ( $count == '' ) {
        add_term_meta( $category_id, $count_key, '0' );
    }
    else {
        $count = intval( $count );
        $count++;
        update_term_meta( $category_id, $count_key, $count );
    }
}


function claudio_massad_track_category_views() {
    if ( is_category() ) {
        $queried_object = get_queried_object();
        $category_id = $queried_object->term_id;
        claudio_massad_set_category_views( $category_id );
    }
}

add_action('wp_head', 'claudio_massad_track_category_views');