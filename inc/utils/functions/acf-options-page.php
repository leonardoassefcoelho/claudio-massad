<?php

function claudio_massad_register_options_page() {

    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page(array(
            'page_title'    => __('Configurações do Blog','claudio_massad'),
            'menu_title'    => __('Configurações do Blog','claudio_massad'),
            'menu_slug'     => 'blog-settings',
            'icon_url'      => 'dashicons-admin-settings'
        ));

        acf_add_options_sub_page(array(
            'page_title'    => __('Opções do Header','claudio_massad'),
            'menu_title'    => __('Header','claudio_massad'),
            'parent_slug'   => 'blog-settings',
        ));
        
        acf_add_options_sub_page(array(
            'page_title'    => __('Opções do Footer','claudio_massad'),
            'menu_title'    => __('Footer','claudio_massad'),
            'parent_slug'   => 'blog-settings',
        ));

        acf_add_options_sub_page(array(
            'page_title'    => __('Opções da Newsletter','claudio_massad'),
            'menu_title'    => __('Newsletter','claudio_massad'),
            'parent_slug'   => 'blog-settings',
        ));

        acf_add_options_sub_page(array(
            'page_title'    => __('Opções de Redes Sociais','claudio_massad'),
            'menu_title'    => __('Redes Sociais','claudio_massad'),
            'parent_slug'   => 'blog-settings',
        ));

        acf_add_options_sub_page(array(
            'page_title'    => __('Opções de Tracking','claudio_massad'),
            'menu_title'    => __('Tracking','claudio_massad'),
            'parent_slug'   => 'blog-settings',
        ));

        acf_add_options_sub_page(array(
            'page_title'    => __('Opções de Categorias Populares','claudio_massad'),
            'menu_title'    => __('Seção de Categorias Populares','claudio_massad'),
            'parent_slug'   => 'blog-settings',
        ));
        
        acf_add_options_sub_page(array(
            'page_title'    => __('Opções da Seção de Produto','claudio_massad'),
            'menu_title'    => __('Seção de Produto','claudio_massad'),
            'parent_slug'   => 'blog-settings',
        ));

        acf_add_options_sub_page(array(
            'page_title'    => __('Opções da Página 404','claudio_massad'),
            'menu_title'    => __('Página 404','claudio_massad'),
            'parent_slug'   => 'blog-settings',
        ));
    
        acf_add_options_sub_page(array(
            'page_title'    => __('Opções da Página de Busca','claudio_massad'),
            'menu_title'    => __('Página de Busca','claudio_massad'),
            'parent_slug'   => 'blog-settings',
        ));
    }

}

add_action( 'after_setup_theme', 'claudio_massad_register_options_page' );