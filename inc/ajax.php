<?php

function claudio_massad_get_searched_posts() {

	$nonce = $_GET['nonce'];
	if( !isset($nonce) || !wp_verify_nonce($nonce, 'search_posts') ) {
		wp_send_json_error (
			array(
				"message" => "Nonce invalid"
			), 
			400
		);
		wp_die();
	}

	$result_array = array(
		'items' => array()
	);

	if( isset($_GET['term']) ) {
		$args['s'] = $_GET['term'];
	}
	else {
		$result_array = array($result_array);
		wp_send_json($result_array);
		wp_die();
	}

	$args['posts_per_page'] = 3;
	
	$searched_query = new WP_Query( $args );

	while( $searched_query->have_posts() ) {
		$searched_query->the_post();
		$result_array['items'][] = claudio_massad_get_post_card_information('search');
	}

	if( $searched_query->found_posts > 3 ) {
		$result_array['show_more'] = true;
		$result_array['show_more_link'] = home_url() . '/?s=' . $_GET['term'];
	}
	
	$result_array = array($result_array);
	wp_send_json($result_array);
	wp_die();

}

add_action( 'wp_ajax_get_searched_posts', 'claudio_massad_get_searched_posts' );
add_action( 'wp_ajax_nopriv_get_searched_posts', 'claudio_massad_get_searched_posts' );

function claudio_massad_get_grid_posts() {

	$nonce = $_GET['nonce'];
	if( !isset($nonce) || !wp_verify_nonce($nonce, 'grid_posts') ) {
		wp_send_json_error (
			array(
				"message" => "Nonce invalid"
			), 
			400
		);
		wp_die();
	}

	$result_array = array(
		'items' => array()
	);

	$url = strpos(site_url(),'localhost') ? "//localhost" : site_url();

	if( isset($_GET['page']) ) {
		$args['paged'] = $_GET['page'];
	}
	else {
		$result_array['url'] = $url . "/wp-admin/admin-ajax.php?action=get_grid_posts&nonce=" . $_GET['nonce'];
		wp_send_json($result_array);
		wp_die();
	}

	if( isset($_GET['tag']) && $_GET['tag'] != '' ) {
		$args['tag'] = $_GET['tag'];
	}

	if( isset($_GET['term']) && $_GET['term'] != '' ) {
		$args['s'] = $_GET['term'];
	}

	if( isset($_GET['cat']) && $_GET['cat'] != '' ) {
		$args['category_name'] = $_GET['cat'];
	}

	if( isset($_GET['author']) && $_GET['author'] != '' ) {
		$args['author'] = $_GET['author'];
	}

	if( isset($_GET['order']) ) {
		if( $_GET['order'] == "date" ) {
			$args['orderby'] = "date";
		}
		if( $_GET['order'] == "views" ) {
			$args['orderby'] = "meta_value_num";
			$args['meta_key'] = "post_view_count";
		}
		if( $_GET['order'] == "comments" ) {
			$args['orderby'] = "comment_count";
		}
	}

	$args['post_status'] = 'publish';

	$grid_query = new WP_Query($args);

	while( $grid_query->have_posts() ) {
		$grid_query->the_post();
		$result_array['items'][] = claudio_massad_get_post_card_information();
	}

	$result_array['count'] = $grid_query->found_posts;

	$result_array['url'] = $url . "/wp-admin/admin-ajax.php?action=get_grid_posts&page=" . (intval($_GET['page'])) . "&nonce=" . $_GET['nonce'] . "&tag=" . $_GET['tag'] . "&term=" . $_GET['term'] . "&cat=" . $_GET['cat'] . "&order=" . $_GET['order'];

	if( $grid_query->max_num_pages < (intval($_GET['page']) + 1) ) {
		$result_array['load-more-src'] = "";
	}
	else {
		$load_more_src = $url . "/wp-admin/admin-ajax.php?action=get_grid_posts&page=" . (intval($_GET['page']) + 1) . "&nonce=" . $_GET['nonce'] . "&tag=" . $_GET['tag'] . "&term=" . $_GET['term'] . "&cat=" . $_GET['cat'] . "&order=" . $_GET['order'];
		$result_array['load-more-src'] = $load_more_src;
	}

	wp_send_json($result_array);
	wp_die();
}

add_action( 'wp_ajax_get_grid_posts', 'claudio_massad_get_grid_posts' );
add_action( 'wp_ajax_nopriv_get_grid_posts', 'claudio_massad_get_grid_posts' );


function claudio_massad_get_comments_list() {

	$nonce = $_GET['nonce'];
	if(!isset( $nonce ) || !wp_verify_nonce($nonce, 'get_comments')) {
		wp_send_json_error(
			array(
				"message" => "Nonce invalid"
			), 
			400
		);
		wp_die();
	}

	$result_array = array(
		'items' => array()
	);

	if(!isset($_GET['page'])) {
		wp_send_json($result_array);
		wp_die();
	}

	$number = get_option('comments_per_page');

	$number_parents = get_comments(
		array(
			'count'=> true, 
			'status' => 'approve', 
			'post_id' => intval($_GET['post_id']),
			'parent' => 0
		)
	);

	$page = intval($_GET['page']);
	$offset = ($page - 1) * $number;

	$args = array (
		'post_id' => intval($_GET['post_id']),
		'number' =>  $number,
		'status' => 'approve',
		'parent' => 0,
		'offset' => $offset
	);
	$comments = get_comments($args);

	foreach ($comments as $comment) {
		$children = $comment->get_children();
		$comments_children = array();

		foreach ($children as $comment_children) {
			$comments_children[] = array(
				'comment_ID' => $comment_children->comment_ID,
				'comment_author' => $comment_children->comment_author,
				'comment_date' => get_comment_date('', $comment_children->comment_ID),
				'comment_content' => $comment_children->comment_content
			);
		}

		$result_array['items'][] = array (
			'comment' => array (
				'comment_ID' => $comment->comment_ID,
				'comment_author' => $comment->comment_author,
				'comment_date' => get_comment_date('', $comment->comment_ID),
				'comment_content' => $comment->comment_content
			), 
			'comments_children' => $comments_children
		);
	}
	
	if (($offset + $number) > $number_parents) {
		$result_array['load-more-src'] = "";
	}
	else {
		$url = strpos(site_url(),'localhost') ? "//localhost" : site_url();
		$load_more_src = $url . "/wp-admin/admin-ajax.php?action=get_comments_list&post_id=" . $_GET['post_id']. "&page=" . (intval($_GET['page']) + 1);
		$result_array['load-more-src'] = $load_more_src;
	}

	wp_send_json($result_array);

	wp_die();
}

add_action('wp_ajax_get_comments_list', 'claudio_massad_get_comments_list');
add_action('wp_ajax_nopriv_get_comments_list', 'claudio_massad_get_comments_list');


function claudio_massad_insert_lead() {

	$nonce = $_POST['nonce'];
	if(!wp_verify_nonce($nonce, 'claudio_massad_insert_lead')) {
		wp_send_json_error(array(
			"message" => "Nonce invalid"
		), 400);
		wp_die();
	}
	
	if( get_field('newsletter-recaptcha-site-key', 'option') && get_field('newsletter-recaptcha-secret-key', 'option') ) {
		$recaptcha = claudio_massad_validation_recaptcha();
		if( !$recaptcha ) {
			wp_send_json_error('error', 400); 
		}
	}

	$response = claudio_massad_sign_up_newsletter();
	
	if($response) {
		wp_send_json_success('success', 200);
	} 
	else {
		wp_send_json_error('error', 400); 
	} 

	wp_die();
	
}

add_action('wp_ajax_claudio_massad_insert_lead', 'claudio_massad_insert_lead');
add_action('wp_ajax_nopriv_claudio_massad_insert_lead', 'claudio_massad_insert_lead');


function claudio_massad_validation_recaptcha() {

	if( isset($_POST['recaptcha_token']) ) {

		$secret_key = get_field('newsletter-recaptcha-secret-key', 'option');
		$site_key = sanitize_text_field($_POST['recaptcha_token']);

		$data_recaptcha = 'response=' . $site_key . '&secret=' . $secret_key;

		$curl_recaptcha = curl_init();
		curl_setopt($curl_recaptcha, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
		curl_setopt($curl_recaptcha, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl_recaptcha, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl_recaptcha, CURLOPT_POSTFIELDS, $data_recaptcha);
		curl_setopt($curl_recaptcha, CURLOPT_HTTPHEADER, array(
	        'Content-Type: application/x-www-form-urlencoded',
	        'Content-Length: ' . strlen($data_recaptcha),
	        'cache-control: no-cache',
	    ));

		$response_recaptcha = curl_exec($curl_recaptcha);
		$response_recaptcha = json_decode($response_recaptcha);

		if( !$response_recaptcha->success || $response_recaptcha->score < 0.5 ) {
			return false;
		}
		else {
			return true;
		}
	}
	else {
		return false;
	}

}


function claudio_massad_sign_up_newsletter() {

	if( isset($_POST['email']) ) {
		$email = sanitize_email($_POST['email']);
	}
	else {
		return false;
	}

	$status_code = 200;

	if( $status_code == 200 ) {
		return true;
	}
	else {
		return false;
	}
}