<?php

function claudio_massad_create_default_taxonomy () {

	$args = array(
		'label' => 'Subcategorias',
		'labels' => array(
			'name' => 'Subcategorias',
			'singular_name' => 'Subcategoria',
			'menu_name' => 'Subcategorias',
			'all_items' => 'Todos as Subcategorias',
			'edit_item' =>  'Editar Subcategoria',
			'view_item' => 'Ver Subcategoria',
			'update_item' => 'Atualizar',
			'add_new_item' => 'Adicionar Subcategoria',
			'search_items' => 'Buscar Subcategorias',
			'not_found' => 'Nenhuma Subcategoria Encontrada'
		),
		'hierarchical' => true,
		'show_in_rest' => true
	);

	register_taxonomy( 'default-taxonomy', 'post' , $args );

}

// add_action( 'init' , 'claudio_massad_create_default_taxonomy' , 0 );