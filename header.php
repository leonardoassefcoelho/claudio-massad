<!DOCTYPE html>
<html <?= get_language_attributes() ?>>

    <head>
    	<meta charset="<?php bloginfo( 'charset' ); ?>" />
        <?php wp_head() ?>
    </head>

    <body <?php body_class() ?>>

		<?php if( get_field('tracking-gtm','option') ): ?>
			<amp-analytics config="https://www.googletagmanager.com/amp.json?id=<?=get_field('tracking-gtm','option')?>" data-credentials="include"></amp-analytics>
		<?php endif; ?>
		
		<header class="header">
			<amp-state id="menu">
				<script type="application/json">
				    {
		      			"open": false
				    }
				</script>
			</amp-state>
			<div class="header-wrapper padding-container">
				<div class="logo">
					<?php
						$featured_img = claudio_massad_get_image_attributes( get_field('logo-header-svg','option')['id'], 'full' );
						if( is_front_page() ) : ?>
						<h1 on="tap:top.scrollTo(duration=600)" role="button" tabindex="0" aria-label="<?= __('Ir Para o Topo da Página' , 'claudio_massad') ?>">
							<amp-img
								class="card-img"
								layout="responsive"
								width="187"
								height="69"
								src="<?= $featured_img['src'] ?>"
								<?= $featured_img['srcset'] ? 'srcset="' . $featured_img['srcset'] . '"' : '' ?>
								alt="<?= $featured_img['alt'] ?>"
								title="<?= $featured_img['title'] ?>"
							></amp-img>
						</h1>
					<?php 
						else : 
					?>
						<a href="<?php bloginfo('url')?>" aria-label="<?= __('Ir Para a Página Inicial' , 'claudio_massad') ?>">
							<amp-img
								class="card-img"
								layout="responsive"
								width="187"
								height="69"
								src="<?= $featured_img['src'] ?>"
								<?= $featured_img['srcset'] ? 'srcset="' . $featured_img['srcset'] . '"' : '' ?>
								alt="<?= $featured_img['alt'] ?>"
								title="<?= $featured_img['title'] ?>"
							></amp-img>
						</a>
					<?php endif; ?>
				</div>
				<div class="wrapper-rigth">
					<div class="menu-list desktop" [hidden]="search.toggle">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'header-menu'
								)
							);
						?>
					</div>
					<?php if( get_field('header-cta','option')['show'] ): ?>
						<a [hidden]="search.toggle" class="cta-btn link-to-site-ga" <?= get_field('header-cta','option')['target'] ? 'target="_blank" rel="noopener"' : '' ?> href="<?= get_field('header-cta','option')['url'] ?>"><?= get_field('header-cta','option')['text'] ?></a>
					<?php endif; ?>
					<div 
						class="toggle-menu mobile"
						[class]="menu.open ? 'toggle-menu active' : 'toggle-menu'"
						aria-label="<?= __('Abrir e Fechar Menu' , 'claudio_massad') ?>"
						on="tap:AMP.setState({ menu: { open : !menu.open } , search : { term : '' } })"
						role="button"
						tabindex="0"
					>
						<span class="burger-icon"></span>
					</div>
					<div class="open-search" aria-label="<?= __('Abrir e Fechar Busca' , 'claudio_massad') ?>" role="button" tabindex="0" on="tap:AMP.setState({ search : { toggle : !search.toggle , term : '' } })">
						<div [hidden]="search.toggle">
							<?= file_get_contents( get_template_directory_uri() . '/assets/search.svg' ) ?>
						</div>
						<div class="toggle-menu active search-desktop" hidden [hidden]="!search.toggle">
							<span class="burger-icon"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="sidebar-wrapper">
				<div class="sidebar-header" [class]="menu.open ? 'sidebar-header opened' : 'sidebar-header'">
					<?php get_template_part('template-part/common/dynamic-search'); ?>
					<div class="menu-list mobile container">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'header-menu'
								)
							);
						?>
					</div>
					<?php if( get_field('header-cta','option')['show'] ): ?>
						<a class="cta-btn link-to-site-ga container" <?= get_field('header-cta','option')['target'] ? 'target="_blank" rel="noopener"' : '' ?> href="<?= get_field('header-cta','option')['url'] ?>"><?= get_field('header-cta','option')['text'] ?></a>
					<?php endif; ?>
				</div>
				<div 
					class="overlay" 
					[class]="menu.open ? 'overlay show' : 'overlay'"
					aria-label="<?= __('Fechar Menu' , 'claudio_massad') ?>"
					on="tap:AMP.setState({ menu: { open : !menu.open } , search : { term : '' } })"
					role="button"
					tabindex="0"
				></div>
			</div>
			<div class="search-desktop">
				<?php get_template_part('template-part/common/dynamic-search'); ?>
			</div>
		</header>
		<div class="anchor" id="top"></div>