<section class="post-comments">
    <span id="anchor_comment" class="anchor"></span>
    <div class="comment-container">
        <p class="comment-title"><?= __('Deixe seu comentário' , 'claudio_massad') ?></p>
        <?php comment_form( array(
            'class_form' => 'comment-form',
            'comment_notes_before' => '',
            'title_reply' => '',
            'title_reply_to' => '',
            'title_reply_before' => '',
            'label_submit' => __('Enviar','claudio_massad'),
            'class_submit' => 'submit comment-submit-ga',
            'fields' => array(
                'author' => '<input aria-label="'. __('Nome','claudio_massad') . '" class="form-author" placeholder="' . __('Nome','claudio_massad') . '" name="author" type="text" required="required">',
                'email'  => '<input aria-label="'. __('E-Mail','claudio_massad') . '" class="form-email" placeholder="' . __('E-Mail','claudio_massad') . '" name="email" type="email" required="required">',
                'cookies' => ''
            ),
            'comment_field' =>'<textarea aria-label="' . __('Comentário','claudio_massad') . '" placeholder="' . __('Comentário','claudio_massad') . '" name="comment" rows="6" required="required"></textarea>',
            'format' => 'html5'
        )); ?>
    </div>
    <?php if( have_comments() ): ?>
        <div class="comment-list-container">
            <p class="comment-title"><?= __('Comentários' , 'claudio_massad') ?></p>
            <amp-state id="comments">
                <script type="application/json">
                    {
                        "comments_amp_list": false,
                        "page" : 1
                    }
                </script>
            </amp-state>
            <ul class="comment-list">
                <?php claudio_massad_get_static_comments( get_the_ID() ); ?>
            </ul>
            <amp-list
                id="dynamic-comment-list"
                load-more="manual"
                src="<?= strpos( site_url() , 'localhost' ) ? '//localhost' : site_url() ?>/wp-admin/admin-ajax.php?action=get_comments_list&post_id=<?= get_the_ID() ?>&nonce=<?= wp_create_nonce('get_comments') ?>"
                [src]="comments.comments_amp_list ? ('<?= strpos(site_url(),'localhost') ? '//localhost' : site_url() ?>/wp-admin/admin-ajax.php?action=get_comments_list&post_id=<?= get_the_ID() ?>&page=' + comments.page + '&nonce=<?= wp_create_nonce('get_comments') ?>') : '<?= strpos(site_url(),'localhost') ? '//localhost' : site_url() ?>/wp-admin/admin-ajax.php?action=get_comments_list&post_id=<?= get_the_ID() ?>&nonce=<?= wp_create_nonce('get_comments') ?>'"
                width="auto"
                height="1"
                layout="fixed-height"
                class="comment-list"
                binding="no"
            >
                <template type="amp-mustache">
                    <ul class="comment-list">
                        <li class='post-comment'>                
                            <p class="comment-author">{{{comment.comment_author}}}</p>
                            <div class="comment-content">
                                <p class="comment-text">{{{comment.comment_content}}}</p>
                                <p class="comment-date">{{{comment.comment_date}}}</p>
                                <div class="comment-reply">
                                    <?= file_get_contents( get_template_directory_uri() . '/assets/reply.svg' ) ?>
                                    <a rel="nofollow" class="comment-reply-link" href="#anchor_comment" on="tap:AMP.setState({'commentform_post_<?= get_the_ID() ?>':{'replyToName' : '{{comment.comment_author}}','values':{'comment_parent':'{{comment.comment_ID}}'}}})">Responder</a>
                                </div>
                            </div>
                        </li>
                        <ul class="children">
                            {{#comments_children}}
                                 <li class='post-comment'>                
                                    <p class="comment-author">{{{comment_author}}}</p>
                                    <div class="comment-content">
                                        <p class="comment-text">{{{comment_content}}}</p>
                                        <p class="comment-date">{{{comment_date}}}</p>
                                    </div>
                                </li>
                            {{/comments_children}}
                        </ul>
                    </ul>
                </template>
                <amp-list-load-more load-more-button class="comments-show-more">
                    <button class="button-show-more"><?= __('Ver Mais' , 'claudio_massad') ?></button>
                </amp-list-load-more>
            </amp-list>
        </div>
    <?php endif; ?>
</section>