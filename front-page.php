<?php get_header(); ?>

<div class="homepage top-header-distance">
    <?php
        get_template_part('template-part/home/hero');
        get_template_part('template-part/home/meet');
        get_template_part('template-part/home/history');
        get_template_part('template-part/home/recent-posts');
        get_template_part('template-part/home/sponsors');
        get_template_part('template-part/home/testimonials');
        get_template_part('template-part/common/product-section');
    ?>
</div>

<?php get_footer(); ?>